package com.cynteka.iris.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableList;

@RestController
public class SequenceController {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@RequestMapping(path="sequences/{sequenceName}/nextval", method=RequestMethod.POST)
	public Long nextVal(@PathVariable("sequenceName") String sequenceName) {
		Long res = jdbcTemplate.queryForObject("select nextval('" + sequenceName + "')", Long.class);
		return res;
	}
	
	@RequestMapping(path="sequences/{sequenceName}", method=RequestMethod.POST)
	public String createSequence(@PathVariable("sequenceName") String sequenceName) {
		jdbcTemplate.execute("create sequence " + sequenceName);
		return sequenceName;
	}
	
	@RequestMapping(path="sequences", method=RequestMethod.GET)
	public List<String> sequences() {
		List<String> res = jdbcTemplate.queryForList("SELECT c.relname FROM pg_class c WHERE c.relkind = 'S'", String.class);
		return res;
	}
}
