package com.cynteka.iris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import lombok.extern.java.Log;

@SpringBootApplication
@EnableScheduling
@PropertySources({
	@PropertySource("classpath:application.yml"),
})
@EnableAutoConfiguration
// (exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@EnableJpaRepositories
// (basePackages={"com.cynteka.lilac.db.model"})
// @Import(RepositoryRestMvcConfiguration.class)

@Log
public class IrisApplication {
	public static void main(String[] args) {
		SpringApplication.run(IrisApplication.class, args);
		log.info("Application Iris has been started");
	}
}
